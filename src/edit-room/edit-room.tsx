import React, {useState} from "react";
import './edit-room.css'

type WeekDay =
    | 'Sun'
    | 'Mon'
    | 'Tue'
    | 'Wed'
    | 'Thu'
    | 'Fri'
    | 'Sat'
let WeekDays: WeekDay[] = [
    'Sun',
    'Mon',
    'Tue',
    'Wed',
    'Thu',
    'Fri',
    'Sat'
]
type State = {
    weeklyTimeslot: WeeklyTimeslot[]
}
type WeeklyTimeslot = {
    'Sun': boolean
    'Mon': boolean
    'Tue': boolean
    'Wed': boolean
    'Thu': boolean
    'Fri': boolean
    'Sat': boolean
    start: string
    end: string
}
let sampleState: State = {
    weeklyTimeslot: [
        {
            'Sun': false,
            'Mon': true,
            'Tue': true,
            'Wed': true,
            'Thu': true,
            'Fri': true,
            'Sat': false,
            start: '10:00',
            end: '20:30'
        },
        {
            'Sun': true,
            'Mon': false,
            'Tue': false,
            'Wed': false,
            'Thu': false,
            'Fri': false,
            'Sat': true,
            start: '05:00',
            end: '08:30'
        },
        {
            'Sun': true,
            'Mon': true,
            'Tue': true,
            'Wed': true,
            'Thu': true,
            'Fri': true,
            'Sat': true,
            start: '20:00',
            end: '22:30'
        },
        {
            'Sun': true,
            'Mon': false,
            'Tue': false,
            'Wed': false,
            'Thu': false,
            'Fri': false,
            'Sat': true,
            start: '05:00',
            end: '08:30'
        },
    ]
}
type WeeklySummary = {
    [weekDay: string]: StartEnd[]
}
type StartEnd = {
    start: string
    end: string
}

function removeDuplicatedStartEnd(timeslots: StartEnd[]): StartEnd[] {
    let hasSeen = new Set<string>()
    return timeslots.filter(({start, end}) => {
        let key = `${start}-${end}`
        if (hasSeen.has(key)) {
            return false
        }
        hasSeen.add(key)
        return true
    })
}

// str format = hh:mm
function strToTime(str: string) {
    let parts = str.split(':')
    let hour = +parts[0]
    let minute = +parts[1]
    return hour * 60 + minute
}

// output format = hh:mm
function timeToStr(time: number) {
    let minute = time % 60
    let hour = (time - minute) / 60
    return d2(hour) + ':' + d2(minute)
}

function mergeStartEnd(timeslots: StartEnd[]): StartEnd[] {
    let availableTimes: boolean[] = []
    for (let i = strToTime('00:00'); i <= strToTime('23:59'); i++) {
        availableTimes[i] = false
    }
    for (let {start, end} of timeslots) {
        for (let i = strToTime(start); i <= strToTime(end); i++) {
            availableTimes[i] = true
        }
    }
    let res: StartEnd[] = []
    let mode = 'find start'
    let start: number
    let end: number
    for (let i = strToTime('00:00'); i <= strToTime('23:59'); i++) {
        if (mode == 'find start' && availableTimes[i]) {
            start = i
            mode = 'find end'
        }
        if (mode == 'find end' && !availableTimes[i]) {
            end = i
            mode = 'find start'
            res.push({
                start: timeToStr(start!),
                end: timeToStr(end - 1)
            })
        }
    }
    if (mode == 'find end') {
        res.push({
            start: timeToStr(start!),
            end: '23:59'
        })
    }
    // check for full-day special case
    if (res.some(timeslot =>
        timeslot.start == '00:00'
        && timeslot.end == '00:00'
    )) {
        return [FullDay]
    }
    return res
}

let FullDay: StartEnd = {
    start: '00:00', end: '00:00'
}

function reduceTimeslot(timeslots: WeeklyTimeslot[]): WeeklySummary {
    let res: WeeklySummary = {}
    for (let weekDay of WeekDays) {
        res[weekDay] = []
    }
    for (let timeslot of timeslots) {
        for (let weekDay of WeekDays) {
            if (timeslot[weekDay]) {
                res[weekDay].push({
                    start: timeslot.start,
                    end: timeslot.end
                })
            }
        }
    }
    for (let weekDay of WeekDays) {
        // res[weekDay] = removeDuplicatedStartEnd(res[weekDay])
        res[weekDay] = mergeStartEnd(res[weekDay])
    }
    return res
}

export default function EditRoom() {
    let [state, setState] = useState(sampleState)
    let weeklySummary = reduceTimeslot(state.weeklyTimeslot)
    return <div className='edit-room'>
        <div>
            <h2>
                Current Weekly Available
            </h2>
            <table className='summary'>
                <thead>
                <tr>
                    <th>
                        Days
                    </th>
                    {WeekDays.map((w, index) => <th key={index}>
                        {w}
                    </th>)}
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Opening Hours</td>
                    {WeekDays.map((weekDay, index) => <td key={index} className='weekday'>
                        {(() => {
                            let timeslots = weeklySummary[weekDay];
                            if (timeslots[0] === FullDay) {
                                return <div className='time'>
                                    Full-day
                                </div>
                            }
                            return timeslots.map((timeslot, index) => <div key={index} className='time'>
                                {timeslot.start} - {timeslot.end}
                            </div>)
                        })()}
                    </td>)}
                </tr>
                </tbody>
            </table>
        </div>
        <div>
            <h2>Edit Zone</h2>
            {state.weeklyTimeslot.map((timeslot, index) => <div key={index} className='timeslot'>
                <div>
                    {WeekDays.map((weekday, index) => <div key={index} className='weekday'>
                        <input type='checkbox' checked={timeslot[weekday]} onChange={e => {
                            let checked = (e.target as HTMLInputElement).checked
                            timeslot[weekday] = checked
                            // timeslot = {...timeslot}
                            // state.weeklyTimeslot[index] = timeslot
                            // state.weeklyTimeslot = [...state.weeklyTimeslot]
                            state = {...state}
                            setState(state)
                        }}/>
                        {weekday}
                    </div>)}
                </div>
                <TimeEditor time={timeslot.start} title={'Start at'} setTime={time => {
                    timeslot.start = time
                    state = {...state}
                    setState(state)
                }}/>
                <TimeEditor time={timeslot.end} title={'End at'} setTime={time => {
                    timeslot.end = time
                    state = {...state}
                    setState(state)
                }}/>
            </div>)}
        </div>
    </div>
}

function TimeEditor(props: {
    title: string
    time: string
    setTime: (time: string) => void
}) {
    // hour: 00 .. 23
    let [hour, minute] = props.time.split(':')
    let hourText = hour
    let M = 'AM'
    if (hour == '00') {
        hourText = '12'
        M = 'AM'
    } else if (hour == '12') {
        hourText = '12'
        M = 'PM'
    } else if (+hour > 12) {
        hourText = d2(+hour - 12)
        M = 'PM'
    }
    return <div className='time'>
        <div>
            {props.title}:
        </div>
        <div>
            <select value={hourText} onChange={e => {
                let value = e.target.value
                if (value == '12' && M == 'AM') {
                    hour = '00'
                } else if (value == '12' && M == 'PM') {
                    hour = '12'
                } else if (M == 'PM') {
                    hour = '' + (+value + 12)
                } else {
                    hour = value
                }
                props.setTime(hour + ':' + minute)
            }}>
                {Hours.map((h, i) => <option key={i}>{h}</option>)}
            </select>
            <select value={minute} onChange={e => {
                let value = e.target.value
                minute = value
                props.setTime(hour + ':' + minute)
            }}>
                {Minutes.map((h, i) => <option key={i}>{h}</option>)}
            </select>
            <select value={M} onChange={e => {
                let value = e.target.value
                M = value
                if (M == 'PM') {
                    hour = '' + ((+hour % 12) + 12)
                } else if (M == 'AM') {
                    hour = '' + (+hour % 12)
                }
                props.setTime(hour + ':' + minute)
            }}>
                <option>AM</option>
                <option>PM</option>
            </select>
        </div>
    </div>
}

let Hours = new Array(12).fill(0)
    .map((_, i) => d2(i + 1))
let Minutes = ['00', '30']

function d2(d: number) {
    if (d < 10) {
        return '0' + d
    }
    return '' + d
}
