import React from 'react';
import './App.css';
import EditRoom from "./edit-room/edit-room";

function App() {
  return (
    <div className="App">
      <header className="App-header">
          <EditRoom/>
      </header>
    </div>
  );
}

export default App;
